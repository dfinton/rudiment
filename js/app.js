var selectAll = function(event) {
    var isChecked = false;

    if (this.checked) {
        isChecked = true;
    }

    $('.select-document').each(function(index, input) {
        $(input).prop('checked', isChecked);
    });
};

var resetSelectAll = function(event) {
    $('.select-all').prop('checked', false);
};

var deleteSelected = function(event) {
    event.preventDefault();

    var documentIdList = [];

    $('.select-document:checked').each(function(index, input) {
        documentIdList.push($(input).val());
    });

    var postData = {
        _token : $('#_token').val(),
        documentIdList : documentIdList
    };

    $.post($(this).attr('href'), postData, function(data) {
         window.location.replace(window.location.href);
    })
};

$(document).ready(function() {
    $('.select-all').change(selectAll);
    $('.delete-selected').click(deleteSelected);
    $('.select-document').change(resetSelectAll);
});
