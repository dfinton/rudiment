<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     * @access protected
     */
    protected $dates = ['deleted_at'];

    /**
     * Helper method to link the users table with the documents table
     *
     * @access public
     * @return User
     */
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
