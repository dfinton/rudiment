<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Validator;

class DocumentController extends Controller
{
    /**
     * Maps the document object into an array containing the data
     * or default null/empty string values if the document result
     * is NULL
     *
     * @access private
     * @param Document $documentResult
     * @return array
     */
    private function documentDataMapper($documentResult, $showTrashed = true) {
        $document = array(
            'id' => null,
            'user_id' => null,
            'title' => '',
            'body' => '',
        );

        if (null !== $documentResult) {
            $user = $documentResult->user;
            $isDeletedClass = '';
            $isDeleted = $documentResult->trashed();
            $isHiddenClass = '';

            if ($isDeleted) {
                $isDeletedClass = 'is-deleted';

                if (null === $showTrashed) {
                    $isHiddenClass = 'hidden';
                }
            }

            $document['id'] = $documentResult->id;
            $document['user_id'] = $documentResult->user_id;
            $document['title'] = $documentResult->title;
            $document['body'] = $documentResult->body;
            $document['is_deleted'] = $isDeleted;
            $document['is_deleted_class'] = $isDeletedClass;
            $document['is_hidden_class'] = $isHiddenClass;
            $document['author_name'] = $user->name;
            $document['author_email'] = $user->email;
        }

        return $document;
    }

    /**
     * Gets the requested document
     *
     * @access public
     * @param Request $request
     * @return String The HTML document
     */
    public function get(Request $request) {
        $documentId = $request->documentId;

        $documentResult = Document::where('id', $documentId)
            ->get()
            ->first();

        $document = $this->documentDataMapper($documentResult);

        return view('document', array(
            'document' => $document,
        ));
    }

    /**
     * Gets a list of documents
     *
     * @access public
     * @param Request $request
     * @return String The HTML document
     */
    public function index(Request $request) {
        $showTrashed = $request->showTrashed;
        $documentResultList = Document::withTrashed()->get();
        $documentList = array();

        foreach ($documentResultList as $documentResult) {
            $documentList[] = $this->documentDataMapper($documentResult, $showTrashed);
        }

        return view('documents', array(
            'documentList' => $documentList,
            'showTrashed' => $showTrashed,
        ));
    }

    /**
     * Brings up the edit screen for a document
     *
     * @access public
     * @param Request $request
     * @return String The HTML document
     */
    public function edit(Request $request) {
        $documentId = $request->documentId;

        $documentResult = Document::where('id', $documentId)
            ->get()
            ->first();

        $document = $this->documentDataMapper($documentResult);

        $userList = User::orderBy('name', 'asc')->get();

        return view('edit-document', array(
            'userList' => $userList,
            'document' => $document,
        ));
    }

    /**
     * Saves the POST data to a new or existing document
     *
     * @access public
     * @param Request $request
     * @return String The HTML document
     */
    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/edit-document')
                ->withInput()
                ->withErrors($validator);
        }

        $documentId = $request->documentId;

        if (null === $documentId) {
            $document = new Document;
        } else {
            $document = Document::where(array('id' => $documentId))->firstOrFail();
        }

        $document->user_id = $request->user;
        $document->title = $request->title;
        $document->body = $request->body;
        $document->save();

        return redirect('/');
    }

    /**
     * Soft deletes the requested document
     *
     * @access public
     * @param Request $request
     * @return String The HTML document
     */
    public function delete(Request $request) {
        $documentIdList = $request->get('documentIdList');

        if (null === $documentIdList) {
            return json_encode(array('numberDeleted' => 0));
        };

        $deletedRows = Document::whereIn('id', $documentIdList)->delete();

        return json_encode(array('numberDeleted' => $deletedRows));
    }

    /**
     * Undeletes the requested document
     *
     * @access public
     * @param Request $request
     * @return String The HTML document
     */
    public function undelete(Request $request) {
        $documentId = $request->documentId;

        Document::where('id', $documentId)->restore();

        return redirect('/index/yes');
    }
}
