<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

use App\Document;
use App\User;
use Illuminate\Http\Request;  

Route::get('/index/{showTrashed?}', 'DocumentController@index');
Route::get('/', 'DocumentController@index');
Route::get('/document/{documentId}', 'DocumentController@get');
Route::get('/edit-document/{documentId?}', 'DocumentController@edit');
Route::get('/undelete-document/{documentId}', 'DocumentController@undelete');
Route::post('/edit-document/{documentId?}', 'DocumentController@update');
Route::post('/delete-document', 'DocumentController@delete');
