@extends('layouts.app')

@section('content')
<input id="_token" type="hidden" name="_token" value="{{ csrf_token() }}" />

<div class="container">
    <div class="row">
        <div class="col-lg-12 centered">
            <div class="button-group">
                <a href="{{ url('/edit-document'), ['null'] }}" class="btn btn-info" role="button">Create Document</a>
                <a href="{{ url('/delete-document') }}" class="btn btn-danger delete-selected" role="button">Delete Selected Documents</a>
            </div>
            <div class="button-group">
                @if ($showTrashed)
                    <a href="{{ url('/') }}" class="btn btn-info" role="button">Hide Deleted Documents</a>
                @else
                    <a href="{{ url('/index', ['yes']) }}" class="btn btn-info" role="button">Show Deleted Documents</a>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="select-column centered">
                            <input type="checkbox" class="select-all" />
                        </th>
                        <th>Title</th>
                        <th class="centered">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($documentList as $document)
                        <tr class="{{ $document['is_deleted_class'] }} {{ $document['is_hidden_class'] }}">
                            <td class="select-column centered">
                                <input class="select-document" type="checkbox" name="document-list" value="{{ $document['id'] }}" />
                            </td>
                            <td>{{ $document['title'] }}</td>
                            <td class="action-column centered">
                                @if ($document['is_deleted'])
                                    <a href="{{ url('/undelete-document', [$document['id']]) }}" class="btn btn-info" role="button">Undelete</a>
                                @else
                                    <a href="{{ url('/document', [$document['id']]) }}" class="btn btn-success" role="button">View</a>
                                    <a href="{{ url('/edit-document', [$document['id']]) }}" class="btn btn-info" role="button">Edit</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>
@endsection
