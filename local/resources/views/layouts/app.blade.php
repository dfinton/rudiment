<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="{{ url('/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ url('/css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ url('/css/app.css') }}" rel="stylesheet">

        <title>Rudiment - Rudimentary CMS Application</title>
    </head>

    <body>
        <div class="container">
            <nav class="navbar navbar-default centered">
                <h1>Welcome to Rudiment</h1>
            </nav>
        </div>

        @yield('content')

        <script src="{{ url('/js/jquery.min.js') }}"></script>
        <script src="{{ url('/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('/js/app.js') }}"></script>
    </body>
</html>
