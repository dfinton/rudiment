@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            {{ $dumpData }}
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>
@endsection
