@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="panel-body">
                <!-- Display Validation Errors -->
                @include('common.errors')

                <!-- New Document Form -->
                <form method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                    <!-- document subject -->
                    <div class="form-group">
                        <label for="user" class="col-sm-3 control-label">User</label>

                        <div class="col-sm-6">
                            <select name="user" id="user" class="form-control">
                                @if (null === $document['user_id'])
                                    <option value="" selected>Please select a User</option>
                                @else
                                    <option value="">Please select a User</option>
                                @endif

                                @foreach ($userList as $user)
                                    @if ($user->id === $document['user_id'])
                                        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                                    @else
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <!-- document subject -->
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label">Title</label>

                        <div class="col-sm-6">
                            <input type="text" name="title" id="title" value="{{ $document['title'] }}" class="form-control"></input>
                        </div>
                    </div>

                    <!-- Document Body -->
                    <div class="form-group">
                        <label for="body" class="col-sm-3 control-label">Body</label>

                        <div class="col-sm-6">
                            <textarea name="body" id="body" rows="10" class="form-control">{{ $document['body'] }}</textarea>
                        </div>
                    </div>

                    <!-- Add Document Button -->
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-plus"></i> Save Document
                            </button>
                            <a href="{{ url('/') }}" class="btn btn-danger" role="button">
                                <i class="fa fa-times"></i> Cancel
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>
@endsection
