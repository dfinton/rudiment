@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 centered">
            <a href="{{ url('/') }}" class="btn btn-success" role="button">Return to Document List</a>
            <a href="{{ url('/edit-document', [$document['id']]) }}" class="btn btn-info" role="button">Edit</a>
        </div>
        <div class="col-lg-3"></div>
    </div>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 centered">
            <h1>{{ $document['title'] }}</h1>
            <h2>by <a href="mailto:{{ $document['author_email'] }}">{{ $document['author_name'] }}</a></h2>
            <div class="document-body">{{ $document['body'] }}</div>
        </div>
        <div class="col-lg-3"></div>
    </div>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 centered">
            <a href="{{ url('/') }}" class="btn btn-success" role="button">Return to Document List</a>
            <a href="{{ url('/edit-document', [$document['id']]) }}" class="btn btn-info" role="button">Edit</a>
        </div>
        <div class="col-lg-3"></div>
    </div>
</div>
@endsection
